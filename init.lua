local obj = {}
obj.__index = obj

-- Metadata
obj.name = 'WindowManager'
obj.homepage = 'https://bitbucket.org/ty-i3/hammerspoon/'

obj.defaultHotkeys = {
  bottom_half = { {"ctrl", "alt", "cmd"}, "Down" },
  left_half   = { {"ctrl", "alt", "cmd"}, "Left" },
  right_half  = { {"ctrl", "alt", "cmd"}, "Right" },
  top_half    = { {"ctrl", "alt", "cmd"}, "Up" },

  full_screen     = { {"shift", "ctrl", "alt", "cmd"}, "Up" },
  previous_screen = { {"shift", "ctrl", "alt", "cmd"}, "Left" },
  next_screen     = { {"shift", "ctrl", "alt", "cmd"}, "Right" }
}

function obj:setWindowCoords(coords)
  -- TODO This moveToUnit function might be simpler    http://www.hammerspoon.org/docs/hs.window.html#moveToUnit
  obj.logger = hs.logger.new('WindowManager')

  local win = hs.window.focusedWindow()
  local winFrame = win:frame()
  local screen = win:screen()
  local screenFrame = screen:frame()
  --self.logger.ef("winFrame: %s", hs.inspect(winFrame))
  --self.logger.ef("screenFrame: %s", hs.inspect(screenFrame))

  winFrame.x = screenFrame.x + screenFrame.w * coords.x
  winFrame.y = screenFrame.y + screenFrame.h * coords.y
  winFrame.w = screenFrame.w * coords.w
  winFrame.h = screenFrame.h * coords.h

  win:setFrame(winFrame, 0)
end

function obj:adjustWindow(fn)
  local win = hs.window.focusedWindow()
  local screen = win:screen()
  fn(win, screen)
end

obj.windowCoords = {
  bottom_half = { x = 0,   y = 0.5, w = 1,   h = 0.5 },
  left_half =   { x = 0,   y = 0,   w = 0.5, h = 1 },
  right_half =  { x = 0.5, y = 0,   w = 0.5, h = 1 },
  top_half =    { x = 0,   y = 0,   w = 1,   h = 0.5 },

  full_screen = { x = 0,   y = 0,   w = 1,   h = 1 }
}

function obj:bindHotkeys(mapping)
  local hotkeyDefinitions = {
    bottom_half = hs.fnutils.partial(obj.setWindowCoords, self, obj.windowCoords.bottom_half),
    left_half = hs.fnutils.partial(obj.setWindowCoords, self, obj.windowCoords.left_half),
    right_half = hs.fnutils.partial(obj.setWindowCoords, self, obj.windowCoords.right_half),
    top_half = hs.fnutils.partial(obj.setWindowCoords, self, obj.windowCoords.top_half),

    full_screen = hs.fnutils.partial(obj.setWindowCoords, self, obj.windowCoords.full_screen),
    previous_screen = hs.fnutils.partial(obj.adjustWindow, self,
      function(win, screen) win:moveToScreen(screen:previous(), 0) end
    ),
    next_screen = hs.fnutils.partial(obj.adjustWindow, self,
      function(win, screen) win:moveToScreen(screen:next(), 0) end
    )
  }

  hs.spoons.bindHotkeysToSpec(hotkeyDefinitions, mapping)
  return self
end

return obj
