1. Install [Hammerspoon](https://www.hammerspoon.org/) onto your Mac:
```
brew cask install hammerspoon
```
and enable Accessibility for Hammerspoon in System Preferences.

2. Clone this repository into your `~/.hammerspoon/Spoons` directory:
```
git clone https://bitbucket.org/ty-i3/WindowManager.spoon ~/.hammerspoon/Spoons/
```

3. Add the following snippet to your `~/.hammerspoon/init.lua`:
```lua
hs.loadSpoon("WindowManager")

spoon.WindowManager:bindHotkeys(spoon.WindowManager.defaultHotkeys)
```

4. Instruct Hammerspoon to reload its configuration. (You can use the Hammerspoon menu in macOS's global menu bar)

And you are finished setting up Hammerspoon and this WindowManager spoon!
Move your windows around the screen using <kbd>command</kbd><kbd>ctrl</kbd><kbd>alt/option</kbd> + cursor keys to move windows, and <kbd>shift</kbd><kbd>command</kbd><kbd>ctrl</kdb><kbd>alt/option</kbd> + <kbd>Up</kbd> to make a window full-screen.

Also, look at the [official repository of spoon plugins](https://github.com/Hammerspoon/Spoons) to get new features and ideas for your own creations!


References
----
- http://www.hammerspoon.org/docs/hs.window.html
